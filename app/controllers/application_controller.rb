class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def home
    @@pkce = Pkce.generate_code_set
    @code_challenge = @@pkce.code_challenge
  end

  def oauth_callback
    response = HisIdp::PKCE.get_access_token(params['code'], @@pkce.code_verifier)
    render plain: response.inspect
  end

  def logout
    @@pkce = nil
    # Eat all the session status
    # Throw a remote sign out
  end
end
