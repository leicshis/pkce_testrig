class HisIdp
  attr_accessor :access_token, :authorization_code

  IDP_URL = 'http://localhost:3000'

  module PKCE
    def self.authorize_path(code_challenge, redirect_uri)
      query_string = {
        client_id: 'd6b313b90de2f693abf69600cdf93366b47182b239ef08418edd0f8f2be6216b',
        response_type: 'code',
        code_challenge: code_challenge,
        # code_challenge: 'E9Melhoa2OwvFrEMTJguCHaoeK1t8URWbuGJSstw-cM',
        code_challenge_method: 'S256',
        redirect_uri: redirect_uri
      }.to_param

      IDP_URL + '/oauth/authorize?' + query_string
    end

    def self.get_access_token(authorization_code, code_verifier)
      response = HTTParty.post(IDP_URL + '/oauth/token', body: {
        client_id: 'd6b313b90de2f693abf69600cdf93366b47182b239ef08418edd0f8f2be6216b',
        grant_type: 'authorization_code',
        code: authorization_code,
        code_verifier: code_verifier,
        # code_verifier: 'dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk',
        redirect_uri: 'http://localhost:3001/oauth_callback'
        })
      response
    end
  end

end
