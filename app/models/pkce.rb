class Pkce
  def self.generate_code_set
    code_verifier = unpadded_b64_encode generate_32octet_sequence
    code_challenge = unpadded_b64_encode Digest::SHA256.digest(code_verifier)
    Struct.new(:code_verifier, :code_challenge).new(code_verifier, code_challenge)
  end

  private

  def self.generate_32octet_sequence
    SecureRandom.random_bytes(32)
    # Alternatively, can do:
    # (0..255).to_a.sample(32).pack('C*')
  end

  def self.unpadded_b64_encode(string)
    padded_result = Base64.urlsafe_encode64(string)
    padded_result.split('=')[0] # Remove any trailing '='
  end
end
